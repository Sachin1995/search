import sys
from transformation.transform import Transform
import re
import logging
"""
    This module provides functionality of reading text from STDIN
    and searches for match found on  text/line based on given regular expressions
    below optional parameter
    -u ( --underscore ) which prints '^' under the matching text
    -c ( --color ) which highlight matching text [1]
    -m ( --machine ) which generate machine-readable output
                  format: file_name:no_line:start_pos:matched_text
"""


class CommandLineReader(Transform):
    def __init__(self):
        logging.basicConfig(level=logging.INFO)

    """
        searches for lines matching regular expression -r (--regex) by taking input from stdin
        and provides o/p based on selected optional paramete
    """
    def search(*cmd_args):
        self, args = cmd_args
        input_list = []
        print("Enter your content. 'Ctrl-D' or 'Ctrl-Z'(Windows) to save it.")
        for line in sys.stdin:
            if 'quit' == line.rstrip():
                break
            input_list.append(line.rstrip())
        logging.info("_____________________________________________")
        try:
            line_no = 0
            payload = {}
            payload.update({"file_name": "INPUT METHOD IS STDIN"})
            for line in input_list:
                line_no += 1
                for match in re.finditer(args.regex, line, flags=0):
                    payload.update({'start': match.start()})
                    payload.update({'end': match.end()})
                    payload.update({'line_no': line_no})
                    Transform.set_data(self, line, payload)
                    logging.info('\n{line}\nAbove line match is found from STDIN and Line number is {lineno}'
                                 .format(line=line.strip(), lineno=line_no))
                    if args.machine:
                        logging.info('\n{machine_format}  (Format: file_name:no_line:start_pos:matched_text)'
                                     .format(machine_format=Transform.get_machine_format(self)))
                    elif args.underscore:
                        Transform.get_underlined_format(self)
                    elif args.color:
                        logging.info('\n{color_format}'
                                     .format(color_format=Transform.get_colored_format(self)))
                    logging.info("_____________________________________________")
        except Exception as e:
            logging.exception(e)
