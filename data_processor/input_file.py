import re
from transformation.transform import Transform
import logging
"""
    This module provides functionality of reading text from File/files
    and searches for match found on  text/line based on given regular expressions
    below optional parameter
    -u ( --underscore ) which prints '^' under the matching text
    -c ( --color ) which highlight matching text [1]
    -m ( --machine ) which generate machine-readable output
                  format: file_name:no_line:start_pos:matched_text
"""


class CustomFileReader(Transform):
    def __init__(self):
        logging.basicConfig(level=logging.INFO)

    """
            searches for lines matching regular expression -r (--regex) by taking input from File/Files
            and provides o/p based on selected optional parameter
    """
    def search(*cmd_args):
        self, args = cmd_args
        for file in args.files:
            try:
                line_no = 0
                payload = {}
                payload.update({"file_name": file})
                file_obj = open(file, "r")
                for line in file_obj:
                    line_no += 1
                    for match in re.finditer(args.regex, line, flags=0):
                        payload.update({'start': match.start()})
                        payload.update({'end': match.end()})
                        payload.update({'line_no': line_no})
                        Transform.set_data(self, line, payload)
                        logging.info('\n{line}\nAbove line match is found in file {filename} and Line number is {'
                                     'lineno} '
                                     .format(line=line.strip(), filename=file, lineno=line_no))
                        if args.machine:
                            logging.info('\n{machine_format}  (Format: '
                                         'file_name:no_line:start_pos:matched_text) '
                                         .format(machine_format=Transform.get_machine_format(self)))
                        elif args.underscore:
                            Transform.get_underlined_format(self)
                        elif args.color:
                            logging.info('\n{color_format}'
                                         .format(color_format=Transform.get_colored_format(self)))
                        logging.info("_____________________________________________")
            except Exception as e:
                logging.exception(e)
