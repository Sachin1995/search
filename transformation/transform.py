import logging
from utility.colorutil import BackgroundColor
"""
    This module provides 3 functionality ie
    1) Coloring a matched text if -c parameter is set 
    2) Underlining a matched text if -u parameter is set 
    3) Provides Machine format  for a matched text if -m  is set as 
       optional parameter
"""


class Transform:
    def __init__(self):
        self.__line_data = None
        self.__payload = None
        logging.basicConfig(level=logging.INFO)

    """
        Sets payload data and matched line 
    """
    def set_data(self, line_data, payload):
        self.__line_data = line_data.strip()
        self.__payload = payload

    """
        Returns a matched line with highlighting matched text 
    """
    def get_colored_format(self):
        start_index = self.__payload['start']
        end_index = self.__payload['end']
        matched_text = self.__line_data[start_index:end_index]
        colored_text = BackgroundColor.OKGREEN + matched_text + BackgroundColor.ENDC
        transformed_data = self.__line_data[:start_index]+colored_text+self.__line_data[end_index:]
        return transformed_data

    """
            Returns a matched line with machine format ie
            format: file_name:no_line:start_pos:matched_text
     """
    def get_machine_format(self):
        start_index = self.__payload['start']
        end_index = self.__payload['end']
        matched_text = self.__line_data[start_index:end_index]
        line_no = self.__payload['line_no']
        transformed_data = self.__payload['file_name']+':'+str(line_no)+":"+str(start_index)+":"+matched_text
        return transformed_data

    """
            Prints a matched line with ^^^^ under a matched text 
    """
    def get_underlined_format(self):
        start_index = self.__payload['start']
        end_index = self.__payload['end']
        line = self.__line_data
        caret = ' '*start_index+ (end_index-start_index)*'^'
        logging.info('\n{line_no}\n{caret_symbol}'.format(line_no=line.strip(), caret_symbol=caret))
