**About Project**

Python script that searches for lines matching regular expression -r (--regex) in file/s -f (--files)

Use STDIN if file/s option wasn’t provided.

The script accepts list optional parameters which are mutually exclusive:
- -u ( --underscore ) which prints '^' under the matching text
- -c ( --color ) which highlight matching text 
- -m ( --machine ) which generate machine-readable output
-                   format: file_name:no_line:start_pos:matched_text


**Command for running project**

- For help

    ``python3 main.py --help``
- Search pattern from file use -f (file optional parameter) and -r (regex) mandatory parameter

    `python3 main.py -r <pattern> -f <filename> <filename> ..`
-  Search pattern and highlight with 3 mutually exclusive parameter(-u, -m, -c)

    `python3 main.py -r <pattern> -f <file> <file>  -m`

- Search pattern using STDIN

    `python3 main.py -r <pattern>`


