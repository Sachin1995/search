import argparse
from data_processor.input_cmd import CommandLineReader
from data_processor.input_file import CustomFileReader

"""
Implements searches for lines matching regular expression -r (--regex) in file/s -f (--files).
Use of STDIN if file/s option wasn’t provided.
Assume that input is ASCII, you don't need to deal with a different encoding.
If a line matches, print it. Please print the file name and the line number for every match.

The script accepts list optional parameters which are mutually exclusive:
-u ( --underscore ) which prints '^' under the matching text
-c ( --color ) which highlight matching text [1]
-m ( --machine ) which generate machine-readable output
                  format: file_name:no_line:start_pos:matched_text


Multiple matches on a single line are allowed, without overlapping.
"""


class Facade:
    """
        Capturing optional parameter
    """
    def __init__(self):
        parser = argparse.ArgumentParser(description='Process Optional Parameter.')
        parser.add_argument('-r', '--regex', metavar='', required=True, help='an regex')
        parser.add_argument('-f', '--files', metavar='', nargs='*', help='an regex')
        group = parser.add_mutually_exclusive_group()
        group.add_argument('-u', '--underscore', action='store_true', help='An underscore')
        group.add_argument('-m', '--machine', action='store_true', help='machine')
        group.add_argument('-c', '--color', action='store_true', help='color')
        self.args = parser.parse_args()
        self.__cmd_reader = CommandLineReader()
        self.__file_reader = CustomFileReader()

    """
            Redirecting to selected functionality based on optional parameter selected
            eg: if optional parameter is -f(file/files) then flow will be followed to file operations
            otherwise it will take input from stdin
    """
    def search_pattern(self):
        if self.args.files is not None:
            return self.__file_reader.search(self.args)
        else:
            return self.__cmd_reader.search(self.args)


"""
    Uses facade design pattern, so that we can hide
    the complexities of the larger system and provides
    a simpler interface to the client
"""
if __name__ == "__main__":
    obj = Facade()
    obj.search_pattern()
